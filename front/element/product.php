<?php $products = $product->all(); ?>

<section>
 	<div class="container">
 		<div class="section-portion">
 			<div class="row">
 				<div class="col-12">
 					<div class="section-heading">
 						<h2>FEATURE Product</h2>
 					</div>
 				</div>
 			</div>
 			<div class="row">
 				<div class="col-12">
				 	<div class="owl-carousel owl-theme" id="owl-one">
						
						<?php foreach ($products as $product): ?>
					    <div class="item product-item mb-5">
					    	<div class="card">
					    		<div class="card-body">
					    			<div class="product">
										<form action="cart.php" method="POST">
											<input type="hidden" value="<?= $product->product_id; ?>" name="product_id">
											<img src="<?=IMG?><?= $product->picture; ?>" alt="image" class="img-fluid">
											<h2><?= $product->title; ?></h2>
											<div class="price-box">
												<del><?= $product->cost  ?></del> <span><?= $product->total_sales; ?></span> 
											</div>
											<div class="product-action-box">
												<button class="btn btn-danger btn-sm" type="submit">Add to bag</button>
												<a href="<?= PRODUCTDETAIL.'product-details.php?id='.$product->product_id;?>">View Details</a>
											</div>
										</form>
									</div>
					    			
					    		</div>								
							</div>	
						</div>
						<?php endforeach; ?>
						
					</div>
 				</div>
 				
 			</div>
 		</div>
 	</div>
 </section>