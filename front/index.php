<?php

    ob_start();
    include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/front/layout/index.php');
    $layout = ob_get_contents();
    ob_end_clean();
    
?>

<?php 

    use Eshop\Banner\Banner;
    use Eshop\Category\Category;
    
    use Eshop\Product\Product;
    use Eshop\Utility\Messages;

    $banner     = new Banner();
    $category   = new Category();
    $product    = new Product();
    
 ?>

<?php   ob_start();?>

<?php if ($massages = Messages::get()):  ?>
    <div class="alert alert-success"><?=  $massages; ?></div>
<?php endif; ?>

<?php
    include_once(FRONTENEMET . 'banner.php'); // slider
    include_once(FRONTENEMET . 'category.php'); // category
    include_once(FRONTENEMET . 'product.php'); // prouduct
    $index = ob_get_contents();
    ob_end_clean();
    echo str_replace("##MAIN_CONTENT##", $index, $layout)


 ?>
