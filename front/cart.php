<?php  

    ob_start();
    include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/front/layout/index.php');
    $layout = ob_get_contents();
    ob_end_clean();
    
?>
<?php 


    use Eshop\Cart\Cart;
    use Eshop\Utility\Messages;

    $cart = new cart();

    $carts = $cart->all($_SESSION['guest_user']);
    // echo "<pre>";
    // var_dump($carts);




 ?>

<?php ob_start(); ?>
<!-- Feature Category section Start -->		
<section>
 	<div class="container">
 		<div class="row">
            <div class="col-12">
                <div class="top_page_link">
                    <h2><a href="">Home</a> > <a href="">categroy one</a> > <a href="">product details</a></h2>
                </div>        
            </div>      
 		</div>
 	</div>
</section>
 <!-- Feature Category section End -->
  
 <!-- Feature Product section Start -->	


<section>
 	<div class="container">
     	<div class="product-list-section-portion pb-4">
           <div class="row">
            <?php if ($carts)
            {

                
             ?>
                 <div class="col-md-9">
                    <div class="view-cart-table">
                        <table class="table table-striped">
                            <thead>
                              <tr>
                                <th>Item</th>
                                <th></th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Subtotal</th>
                              </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    foreach ($carts as $cart) :
                                 ?>
                                <tr>
                                    <td><a href=""><?= $cart->product_title;?></a> </td>
                                    <td> <img src="<?=IMG?><?=$cart->picture;  ?>" alt="image" width="100px" height="auto"></td>
                                    <td><?=$cart->unite_price;  ?></td>
                                    <td><?=$cart->qty;  ?></td>
                                    <td><?=$cart->total_price;  ?></td>                     
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                 </div>
             <div class="col-md-3">
                <div class="cart-summary">
                    <div class="card">
                        <div class="card-body">
                            <h2>Summary</h2>
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td>Lorem Ipsum </td>
                                            <td>400tk</td>                     
                                        </tr>
                                        <tr>
                                            <td>Lorem Ipsum </td>
                                            <td>400tk</td>                     
                                        </tr>
                                        <tr>
                                            <td><strong>Total</strong> </td>
                                            <td><strong>4000tk</strong></td>
                                        </tr>
                                    </tbody>
                                </table>
                            <div class="proceed-checkout">
                                <a href="#">Preceed to Checkout</a>
                            </div>
                        </div>                
                    </div>  
                </div>
             </div>
<?php 
    }
    else{
        // header('location: index.php');

        echo "<p>You have no items in your shopping cart.</p>";
        echo "<p><a href='".WEBROOT."index.php'> Click here</a> to continue shopping.</p>";
    }

 ?>
           </div>
        </div>
 	</div>


</section>
 <!-- Feature Prduct section End -->

<?php

    $cart = ob_get_contents();
    ob_end_clean();
    echo str_replace("##MAIN_CONTENT##", $cart, $layout)


 ?>