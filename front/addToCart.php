<?php include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/bootstrap.php'); ?>

<?php

    use Eshop\Cart\Cart;
    use Eshop\Product\Product;
    use Eshop\Utility\Messages;

if (isset($_POST) && !empty($_POST)) {
    

    
    $id = $_POST['product_id'];

    
    $products = new Product();

    $product =$products->show($id);

    $data = $_POST;

    $data['sid'] = $_SESSION['guest_user'];
    $data['product_id'] = $product->product_id;
    $data['product_title'] = $product->title;
    $data['picture'] = $product->picture;
    $data['unite_price'] = $product->mrp;
    $data['total_price'] = $product->mrp * $_POST['qty'];
    $data['qty'] = $_POST['qty'];

    // echo "<pre>";
    // var_dump($data);
    // die();
    
    $cart = new Cart();
    $cart->store($data);

}else {
    header('location: index.php');
}

 ?>

