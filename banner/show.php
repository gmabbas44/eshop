<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
      <link rel="manifest" href="site.webmanifest">
      <link rel="apple-touch-icon" href="icon.png">
      <!-- Place favicon.ico in the root directory -->
      <!-- Font Awesome -->
      <link rel="stylesheet" href="../css/all.min.css">

      <!-- Bootstrap -->
      <link rel="stylesheet" href="../css/bootstrap.min.css">

      <!-- Main Stylesheet -->
      <link rel="stylesheet" href="../css/main.css"> 


    <title>Admin Panel</title>
  </head>
  <body>
   
   <div class="body-wrapper"> <!-- Full body wrap -->

     	<aside id="sidebar"> <!-- Sidebar Start  -->
        <div class="sidebar-body">
       		<div class="sidebar-header">
       			<div class="logo-section">
              <img src="../img/logo-white.png" alt="" style="width:140px;">
            </div>
            <!-- admin Info -->
            <div class="admin-info ">
                <div class="admin-image">
                    <img src="../img/jishat.jpg" width="48px" height="48px" alt="User" />
                </div>
                <div class="info-container">
                    <h2>AR Jishat</h2>
                    <h3>arjishat@gmail.com</h3>
                </div>
            </div>
       		</div>   		
       		<nav class="sidebar-menu">
            <ul class="list-unstyled ">
              <h2>MAIN NAVIGATION</h2>
              <li>
                <a href="#"><i class="fas fa-tachometer-alt"></i> Dashboard</a>
              </li>
              <li>
                <a href="javascript:void(0)" data-toggle="collapse" class="dropDownMenu" data-target="#subMenuOne" aria-expanded="false"> <i class="fas fa-tachometer-alt"></i> Home Page</a>
                <ul  id="subMenuOne" class="collapse list-unstyled">
                  <li>
                    <a href="#">Logo</a>
                  </li>
                  <li>
                    <a href="#">Banner</a>
                  </li>
                </ul> 
              </li>              
              <li>
                <a href="view.php"><i class="fas fa-tachometer-alt"></i> Banner</a>
              </li>
              <li>
                <a href="#"><i class="fas fa-tachometer-alt"></i> About</a>
              </li>
              <li>
                <a href="javascript:void(0)" data-toggle="collapse" data-target="#subMenuTwo"> <i class="fas fa-tachometer-alt"></i> Home</a>
                <ul class="collapse list-unstyled" id="subMenuTwo">
                  <li>
                    <a href="#">home1</a>
                  </li>
                  <li>
                    <a href="#">home2</a>
                  </li>
                  <li>
                    <a href="#">home3</a>
                  </li>
                </ul> 
              </li> 
          </ul> 
          </nav>       		
        </div>

        <div class="sidebar-button">
          <button type="button" id="sidebarCollapse" class="btn btn-info">
            <i class="fa fa-align-justify"></i> <span></span>
          </button>  
        </div>        
     	</aside>
      
      
   	<!-- Sidebar Start  End-->

     	<div id="body-content"> <!-- body content start -->

        <div class="tob-bar">
          <div class="container-fluid">
            <div class="row">
              <div class="col-12">
                <nav class="navbar navbar-light ">
                  <ul class="navbar-nav tob-bar-menu">
                      <li><a href="#"> <i class="far fa-bell"></i></a></li>
                      <li><a href="#">Sign Out</a> </li>
                  </ul>
                </nav>
              </div>
            </div>
       	  </div>
        </div>

        <div class="table-section shadow">
          <div class="container-fluid">
            <div class="row mb-3 overflow-hidden">
              <div class="col-8">
                <h2 class="display-5"> View Banner</h2>
              </div>
              <div class="col-4">
                <a href ="index.html" class="main-button" >Go Back</a>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <div class="view-details">
                  <img src="../img/feature-ad-9.jpg" alt="banner" width="80%" height="auto">
                  <h2>Title goes here</h2>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                  <p class="text-dark">Is Active: <span>Yes</span></p>
                </div>
              </div>
            </div>
          </div>
        </div>
  
   	  </div><!-- body content End -->  
   	
   	
  </div> <!-- Full body wrap ENd-->
    

    <!--  JavaScript link -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/main.js"></script>
  
    
    
    
    
    
  </body>
</html>