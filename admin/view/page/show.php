<?php $conn = new PDO("mysql:host=localhost;dbname=laracom","root",""); ?>

<?php
  ob_start();
  include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/admin/view/layout/index.php');
  $layout = ob_get_contents();
  ob_end_clean();
?>

<?php 
  ob_start();
 ?>

<?php 
// Selecting query
$query = "SELECT * FROM banners WHERE banner_id= ".$_GET['id'];
$sth = $conn->prepare($query);
$sth->execute();

$banner = $sth->fetch(PDO::FETCH_ASSOC);
?>



        <div class="tob-bar">
          <div class="container-fluid">
            <div class="row">
              <div class="col-12">
                <nav class="navbar navbar-light ">
                  <ul class="navbar-nav tob-bar-menu">
                      <li><a href="#"> <i class="far fa-bell"></i></a></li>
                      <li><a href="#">Sign Out</a> </li>
                  </ul>
                </nav>
              </div>
            </div>
       	  </div>
        </div>

        <div class="table-section shadow">
          <div class="container-fluid">
            <div class="row mb-3 overflow-hidden">
              <div class="col-8">
                <h2 class="display-5"> View Banner </h2>
              </div>
              <div class="col-4">
                <a href ="index.php" class="main-button" >Go Back</a>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <div class="view-details">
                  <img src="<?php echo $banner['picture']; ?>" alt="banner" width="80%" height="auto">
                  <h2><?php echo $banner['title']; ?></h2>
                  <p><?php echo $banner['promotional_message']; ?></p>
                  <p class="text-dark">Is Active: <span>Yes</span></p>
                </div>
              </div>
            </div>
          </div>
        </div>
  



<?php 

  $create = ob_get_contents();
  ob_end_clean();
  echo str_replace("##MAIN_CONTENT##", $create, $layout)

 ?>