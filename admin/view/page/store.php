<?php 

$bannerTitle = $_POST['bannerTitle'];
$bannerPicture = $_POST['bannerPicture'];
$promotionalMessage = $_POST['promotionalMessage'];

$servername = "localhost";
$username = "root";
$password = "";
$dbname ="laracom";

// Connecting database
$conn = new PDO("mysql:host=$servername;dbname=$dbname;", $username, $password);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


// Selecting query
$query = "INSERT INTO `banners` (
	`title`, 
	`picture`, 
	`link`, 
	`promotional_message`, 
	`html_banner`, 
	`is_active`,
	 `is_draft`, 
	 `soft_delete`, 
	 `max_display`,
	  `created_at`, 
	  `modified_at`) VALUES (
	  :title, 
	  :picture, 
	  NULL,
	  :promotional_message, NULL, NULL, NULL, NULL, NULL, NULL, NULL);";
$sth = $conn->prepare($query);
$sth->bindparam(':title', $bannerTitle);
$sth->bindparam(':picture', $bannerPicture);
$sth->bindparam(':promotional_message', $promotionalMessage);
$result = $sth->execute();

// redirect the page

header("location:index.php");


