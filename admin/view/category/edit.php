<?php $conn = new PDO("mysql:host=localhost;dbname=laracom","root",""); ?>

<?php
  ob_start();
  include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/admin/view/layout/index.php');
  $layout = ob_get_contents();
  ob_end_clean();
?>

<?php 
  ob_start();
 ?>

<?php
    $id = $_GET['id'];
    $query = "SELECT * FROM categories WHERE category_id = $id";
    $results = $conn->prepare($query);
    $results->execute();
    $category = $results->fetch(PDO::FETCH_OBJ);

?>


        <div class="table-section shadow">
          <div class="container-fluid">
            <div class="row mb-3 overflow-hidden">
              <div class="col-12">
                <h2> Edit Category</h2>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <div class="add-form">
                  <form id="contact-form" method="post" action="update.php" role="form">
                    <input type="hidden" name="category_id" value="<?= $category->category_id; ?>">

                    <div class="messages"></div>                    
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input id="name"  value="<?= $category->name; ?>" type="text" name="name" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="link">Link</label>
                                <input id="link"  value="<?= $category->link; ?>" type="text" name="link" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="isDraft">Is Draft</label>
                                <?php 
                                    if ($category->is_draft == 1 ) {
                                        $checkbox = 'checked';
                                    }else{
                                        $checkbox = '';
                                    }
                                 ?>
                                <input id="isDraft" <?= $checkbox; ?>  value="1" type="checkbox" name="is_draft" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-info">Save</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

      
<?php 

  $edit_category = ob_get_contents();
  ob_end_clean();
  echo str_replace("##MAIN_CONTENT##", $edit_category, $layout)

 ?>