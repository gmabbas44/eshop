<?php $conn = new PDO("mysql:host=localhost;dbname=laracom","root",""); ?>

<?php
  ob_start();
  include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/admin/view/layout/index.php');
  $layout = ob_get_contents();
  ob_end_clean();
?>

<?php 
  ob_start();
 ?>

<?php

    $id = $_GET['id'];
    $query = "SELECT * FROM categories WHERE category_id = $id";
    $results = $conn->prepare($query);
    $results->execute();
    $categories = $results->fetch(PDO::FETCH_OBJ);

?>


        <div class="table-section shadow">
          <div class="container-fluid">
            <div class="row mb-3 overflow-hidden">
              <div class="col-8">
                <h2 class="display-5"> View Category</h2>
              </div>
              <div class="col-4">
                <a href ="index.php" class="main-button" >Go Back</a>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <div class="view-details">
                  <h2><?= $categories->name;?></h2>
                  <p><?= $categories->link;?></p>
                  <p class="text-dark">Is Active: <span>Yes</span></p>
                </div>
              </div>
            </div>
          </div>
        </div>
  
<?php 

  $show = ob_get_contents();
  ob_end_clean();
  echo str_replace("##MAIN_CONTENT##", $show, $layout)

 ?>