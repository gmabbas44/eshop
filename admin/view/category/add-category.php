<?php
  ob_start();
  include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/admin/view/layout/index.php');
  $layout = ob_get_contents();
  ob_end_clean();
?>

<?php
use Eshop\Utility\Messages;
  ob_start();
 ?>

        <div class="table-section shadow">
          <div class="container-fluid">
            <div class="row mb-3 overflow-hidden">
              <div class="col-8">
                <h2> Add Category</h2>
              </div>
              <div class="col-4">
                <a href ="index.php" class="main-button" >View Category</a>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <div class="add-form">
                  <form id="contact-form" method="post" action="store.php" role="form">

                    <div class="messages"></div>                    
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input id="name"  value="" type="text" name="name" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="link">Link</label>
                                <input id="link"  value="" type="text" name="link" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="isDraft">Is Draft</label>
                                <input id="isDraft"  value="1" type="checkbox" name="is_draft" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-info">Save</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

       
<?php 

  $add_cat = ob_get_contents();
  ob_end_clean();
  echo str_replace("##MAIN_CONTENT##", $add_cat, $layout)

 ?>