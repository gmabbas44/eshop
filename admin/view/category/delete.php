<?php include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/bootstrap.php'); ?>

<?php
use Eshop\Category\Category;
use Eshop\Utility\Messages;
$categories = new Category();

if (isset($_POST['deleteId']) && !empty($_POST['deleteId'])) {
    $id = $_POST['deleteId'];
    $delete = $categories->delete($id);
}else{
    header('location: index.php');
}

if (isset($_GET['softDeleteid'])) {
    echo $_GET['softDeleteid'];
}

?>