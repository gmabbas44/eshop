<?php include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/bootstrap.php'); ?>

<?php

    use Eshop\Category\Category;
    use Eshop\Utility\Messages;
    $categories = new Category();
    if (isset($_POST) && !empty($_POST))
    {
        $data 		= $_POST;
        $categories->store($data);
    }
    else{
        header("location: add-category.php");
    }


?>


