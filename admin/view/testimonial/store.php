<?php 

$testimonialPicture = $_POST['testimonialPicture'];
$testimonialBody = $_POST['testimonialBody'];
$testimonialName = $_POST['testimonialName'];
$testimonialDesignation = $_POST['testimonialDesignation'];

$servername = "localhost";
$username = "root";
$password = "";
$dbname ="jishop";

// Connecting database
$conn = new PDO("mysql:host=$servername;dbname=$dbname;", $username, $password);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


// Selecting query
$query = "INSERT INTO `testimonials` (
  `picture`, 
  `body`, 
  `name`, 
  `designation`, 
  `is_active`, 
  `is_draft`, 
  `soft_delete`, 
  `created_at`, 
  `modified_at`) VALUES (
  :picture, :body, :name, :designation, NULL, NULL, NULL, NULL, NULL);";
$sth = $conn->prepare($query);
$sth -> bindparam(':picture', $testimonialPicture);
$sth -> bindparam(':body', $testimonialBody);
$sth -> bindparam(':name', $testimonialName);
$sth -> bindparam(':designation', $testimonialDesignation);
$result = $sth->execute();
$sth=null;
// redirect the page

header("location:index.php");


