<?php

$testimnonialId = $_POST['testimnonialId'];
$testimonialPicture = $_POST['testimonialPicture'];
$testimonialBody = $_POST['testimonialBody'];
$testimonialDesignation = $_POST['testimonialDesignation'];

$servername = "localhost";
$username = "root";
$password = "";
$dbname ="jishop";

// Connecting database
$conn = new PDO("mysql:host=$servername;dbname=$dbname;", $username, $password);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


// Selecting query
$query = "UPDATE `testimonials` SET `picture` = :picture `body` = :body, `name` = :name, `designation` = :designation WHERE `testimonials`.`id` = :id;";

$sth = $conn->prepare($query);
$sth -> bindparam(':id', $testimnonialId);
$sth -> bindparam(':picture', $testimonialPicture);
$sth -> bindparam(':body', $testimonialBody);
$sth -> bindparam(':designation', $testimonialDesignation);
$sth->execute();
// redirect the page

header("location:index.php");


