<?php include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/bootstrap.php'); ?>

<?php
	use Eshop\Tag\Tag;
    use Eshop\Utility\Messages;
    $tag = new Tag();
	
	
	if (isset($_GET['id']) && !empty($_GET['id'])) 
	{
		$id = $_GET['id'];
		$tag->softDelete($id);
	}
	else
	{
		header('location: index.php');
	}

?>