    <?php
        include_once($_SERVER['DOCUMENT_ROOT'].'/eshop/bootstrap.php');
        use Eshop\Page\Page;
        use Eshop\Utility\Messages;

        $page = new Page();

       if (isset($_POST) && !empty($_POST)) 
       {
            $imgPath    = DOCROOT.'/eshop/img/';
            $data       = $_POST;
            $ImgName    = $_FILES['picture']['name'];
            $ImageTmp   = $_FILES['picture']['tmp_name'];

            $imgExt     = pathinfo($ImgName, PATHINFO_EXTENSION);
            $img        = uniqid().'.'.$imgExt;

            if(empty($ImgName)){
                $data['picture'] = $data['picture'];
            }else{
                $data['picture'] = $img;
                move_uploaded_file($ImageTmp, $imgPath.$data['picture']);
            }
            $page->update($data);
       }
       else
       {
            header('location: index.php');
       }

    ?>