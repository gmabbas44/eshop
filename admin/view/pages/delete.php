<?php include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/bootstrap.php'); ?>

<?php
	use Eshop\Page\Page;
    use Eshop\Utility\Messages;
    $page = new Page();
	
	if (isset($_POST['deleteId']) && !empty($_POST['deleteId'])) {
		$id = $_POST['deleteId'];
		$delete = $page->delete($id);
	}else{
		header('location: index.php');
	}

?>