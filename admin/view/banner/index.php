<?php $conn = new PDO("mysql:host=localhost;dbname=laracom","root",""); ?>

<?php
  ob_start();
  include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/admin/view/layout/index.php');
  $layout = ob_get_contents();
  ob_end_clean();
?>

<?php 
  ob_start();
?>
<?php 

// Selecting query
$query = "SELECT * FROM banners";
$sth = $conn->prepare($query);
$sth->execute();

$banners = $sth->fetchAll(PDO::FETCH_ASSOC);
?>

<div class="table-section shadow">
    <div class="container-fluid">
        <div class="row mb-3 overflow-hidden">
            <div class="col-8">
                <h2> View Banner</h2>
            </div>
            <div class="col-4">
                <a href="create.php" class="main-button">Add New</a>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="view-table">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Picture</th>
                                <th>Promotional Message</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($banners as $banner) { ?>
                            <tr>
                                <td><a href="show.php?id=<?= $banner['banner_id']; ?> "> <?= $banner['title']; ?> </a> </td>
                                <td> <img src="<?= IMG.$banner['picture']; ?> " alt="image" width="100px" height="auto"></td>
                                <td><?= $banner['promotional_messages']; ?></td>
                                <td><a href="edit.php?id=<?= $banner['banner_id']; ?>">Edit</a> | <a href="delete.php?id=<?= $banner['banner_id']; ?>">Delete</a></td>
                            </tr>
                            <?php }  ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php 

  $show_banners = ob_get_contents();
  ob_end_clean();
  echo str_replace("##MAIN_CONTENT##", $show_banners, $layout)

 ?>