<?php $conn = new PDO("mysql:host=localhost;dbname=laracom","root",""); ?>

<?php
  ob_start();
  include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/admin/view/layout/index.php');
  $layout = ob_get_contents();
  ob_end_clean();
?>

<?php 
  ob_start();
 ?>

        <div class="table-section shadow">
          <div class="container-fluid">
            <div class="row mb-3 overflow-hidden">
              <div class="col-12">
                <h2> Add New</h2>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <div class="add-form">
                  <form id="contact-form" method="post" action="store.php" role="form">

                    <div class="messages"></div>                    
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="bannerTitle">Title</label>
                                <input id="bannerTitle"  value="" type="text" name="bannerTitle" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="bannerPicture">picture</label>
                                <input id="bannerPicture"  value="" type="text" name="bannerPicture" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="promotionalMessage">Promotional Message</label>
                                <input id="promotionalMessage"  value="" type="text" name="promotionalMessage" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="is_active">is_active</label>
                                <input id="is_active"  value="" type="text" name="is_active" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="created_at">created_at</label>
                                <input id="created_at"  value="" type="text" name="created_at" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="modified_at">modified_at</label>
                                <input id="modified_at"  value="" type="text" name="modified_at" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-info">Save</button>
                    <!--<input type="submit" class="btn btn-success btn-send" value="Send & Save message">-->
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

<?php 

  $add_banner = ob_get_contents();
  ob_end_clean();
  echo str_replace("##MAIN_CONTENT##", $add_banner, $layout)

 ?>