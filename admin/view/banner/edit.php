<?php
  ob_start();
  include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/admin/view/layout/index.php');
  $layout = ob_get_contents();
  ob_end_clean();
?>

<?php 
  ob_start();
 ?>

<?php 
// Selecting query
$query = "SELECT * FROM banners WHERE id= ".$_GET['id'];
$sth = $conn->prepare($query);
$sth->execute();

$banner = $sth->fetch(PDO::FETCH_ASSOC);
?>

        <div class="tob-bar">
          <div class="container-fluid">
            <div class="row">
              <div class="col-12">
                <nav class="navbar navbar-light ">   
                  <ul class="navbar-nav tob-bar-menu">
                      <li><a href="#"> <i class="far fa-bell"></i></a></li>
                      <li><a href="#">Sign Out</a> </li>
                  </ul>
                </nav>
              </div>
            </div>
       	  </div>
        </div>

        <div class="table-section shadow">
          <div class="container-fluid">
            <div class="row mb-3 overflow-hidden">
              <div class="col-12">
                <h2> Add New</h2>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <div class="add-form">
                  <form id="contact-form" method="post" action="update.php" role="form">
                    <input   value="<?php echo $banner['id']; ?>" type="hidden" name="bannerId" class="form-control">
                    <div class="messages"></div>                    
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="bannerTitle">Title</label>
                                <input id="bannerTitle"  value="<?php echo $banner['title']; ?>" type="text" name="bannerTitle" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    
                        
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="label_id">label_id</label>
                                <input id="label_id"  value="" type="text" name="label_id" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="title">title</label>
                                <input id="title"  value="" type="text" name="any" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="picture">picture</label>
                                <input id="picture"  value="" type="text" name="picture" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="short_description">short_description</label>
                                <input id="short_description"  value="" type="text" name="short_description" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="description">description</label>
                                <input id="description"  value="" type="text" name="description" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="total_sales">total_sales</label>
                                <input id="total_sales"  value="" type="text" name="total_sales" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="product_type">product_type</label>
                                <input id="product_type"  value="" type="text" name="product_type" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="is_new">is_new</label>
                                <input id="is_new"  value="" type="text" name="is_new" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="cost">cost</label>
                                <input id="cost"  value="" type="text" name="cost" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="mrp">mrp</label>
                                <input id="mrp"  value="" type="text" name="mrp" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="special_price">special_price</label>
                                <input id="special_price"  value="" type="text" name="special_price" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="soft_delete">soft_delete</label>
                                <input id="soft_delete"  value="" type="text" name="soft_delete" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="is_draft">is_draft</label>
                                <input id="is_draft"  value="" type="text" name="is_draft" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="is_active">is_active</label>
                                <input id="is_active"  value="" type="text" name="is_active" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="created_at">created_at</label>
                                <input id="created_at"  value="" type="text" name="created_at" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="modified_at">modified_at</label>
                                <input id="modified_at"  value="" type="text" name="modified_at" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-info">Update</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

       
<?php 

  $edit_banner = ob_get_contents();
  ob_end_clean();
  echo str_replace("##MAIN_CONTENT##", $edit_banner, $layout)

 ?>