<?php include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/bootstrap.php'); ?>

<?php
	use Eshop\Product\Product;
    use Eshop\Utility\Messages;
    $product = new Product();
	
	
	if (isset($_GET['id']) && !empty($_GET['id'])) 
	{
		$id = $_GET['id'];
		$product->softDelete($id);
	}
	else
	{
		header('location: index.php');
	}

?>