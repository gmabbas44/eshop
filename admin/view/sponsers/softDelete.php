<?php include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/bootstrap.php'); ?>

<?php
	use Eshop\Sponser\Sponser;
    use Eshop\Utility\Messages;
    $sponser = new Sponser();
	
	
	if (isset($_GET['id']) && !empty($_GET['id'])) 
	{
		$id = $_GET['id'];
		$sponser->softDelete($id);
	}
	else
	{
		header('location: index.php');
	}

?>