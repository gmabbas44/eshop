
<?php

    ob_start();
    include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/admin/view/layout/index.php');
    $layout = ob_get_contents();
    ob_end_clean();

?>

<?php ob_start(); ?>

<?php

    use Eshop\Sponser\Sponser;
    use Eshop\Utility\Messages;

    $sponser = new Sponser();
    $sponsers = $sponser->all();

 ?>
        <div class="table-section shadow">
          <div class="container-fluid">
            <div class="row mb-3 overflow-hidden">
              <div class="col-8">
                <h2> View sponser</h2>
              </div>
              <div class="col-4">
                <a href ="add-sponser.php" class="main-button" >Add New</a>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <div class="view-table">

                    <?php if ($messages = Messages::get()):  ?>
                        <div class="alert alert-success"><?= $messages; ?></div>
                    <?php endif; ?>

                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th width="30%">Title</th>
                        <th width="10%">Picture</th>
                        <th width="30%">link</th>
                          <th width="50%">Promotional_message</th>
                          <th width="10%"> html_banner</th>
                          <th width="10%"> is_active</th>
                          <th width="10%"> is_draft</th>
                          <th width="10%"> created_at</th>
                          <th width="10%"> modified_at </th>
                          <th width="10%">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php

                        foreach ($sponsers as $sponser):

                    ?>
                        <tr>
                            <td><a href="#"> <?= $sponser->title; ?></a> </td>
                            <td> 
                                <img src="<?= IMG.$sponser->picture; ?>" alt="image" width="100" height="60">
                            </td>
                            <td>
                                <?= $sponser->link; ?>
                            </td>
                            <td> <?= $sponser->promotional_message; ?>
                            </td>
                            <td> <?= $sponser->html_banner ; ?>
                            </td>
                            <td> <?= $sponser-> is_active; ?>
                            </td>
                            <td> <?= $sponser->is_draft ; ?>
                            </td>
                            <td> <?= $sponser-> created_at ; ?>
                            </td>
                            <td> <?= $sponser-> modified_at; ?>
                            </td>
                            <td>
                            <td>
                                <a class="btn btn-primary btn-sm text-white" href="show.php?id=<?= $sponser->sponser_id; ?>"><i class="far fa-eye"></i></a>
                                <a class="btn btn-success btn-sm text-white" href="edit.php?id=<?= $sponser->sponser_id; ?>"><i class="far fa-edit"></i></a>
                                <form action="delete.php" method="POST">
                                    <input type="hidden" value="<?= $sponser->sponser_id; ?>" name="deleteId">
                                    <button class="btn btn-sm btn-danger text-white" onclick="javascript: return confirm('Are you confirm delete record?')"><i class="far fa-trash-alt"></i></button>
                                </form>
                            </td>
                        </tr>
                    <?php
                    endforeach;
                    ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>


<?php 

  $index = ob_get_contents();
  ob_end_clean();
  echo str_replace("##MAIN_CONTENT##", $index, $layout)

 ?>