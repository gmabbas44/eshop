<?php
    include_once($_SERVER['DOCUMENT_ROOT'].'/eshop/bootstrap.php');
    use Eshop\Brand\Brand;
    use Eshop\Utility\Messages;

    $brand = new Brand();

    if (isset($_POST) && !empty($_POST)) 
    {
        
        $data = $_POST;
        $brand->update($data);
    }
    else
    {
        header('location: index.php');
    }

?>