<?php

    ob_start();
    include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/admin/view/layout/index.php');
    $layout = ob_get_contents();
    ob_end_clean();

?>

<?php ob_start(); ?>

<?php

    use Eshop\Brand\Brand;
    use Eshop\Utility\Messages;

    $brand = new Brand();
    $brands = $brand->trash();
 ?>
<form action="" method="">
        <div class="table-section shadow">
          <div class="container-fluid">
            <div class="row mb-3 overflow-hidden">
              <div class="col-8">
                <h2> Trash Brands</h2>
              </div>
              <div class="col-4">
                <a href ="add-product.php" class="main-button" >Add New</a>
              </div>
            </div>
            <label for="select">Check Status</label>
            <div class="row">
                <div class="col-md-3">
                    <select class="form-control" name="" id="select">
                        <option>Select one</option>
                        <option value="delete">Delete</option>
                        <option value="">Restore</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <button type="submit" name="tatus" class="btn btn-info">Apply</button>
                </div>
            </div>
            <div class="row">
              <div class="col-12">
                <div class="table-responsive">

                    <?php if ($messages = Messages::get()):  ?>
                        <div class="alert alert-success"><?= $messages; ?></div>
                    <?php endif; ?>

                  <table id="dataTable" class="table table-striped">
                    <div class="custom-control custom-checkbox m-1">
                        <input class="custom-control-input"  type="checkbox" id="selectAllBoxes">
                        <label class="checkbox custom-control-label text-muted" for="selectAllBoxes">
                        Select All 
                        </label>
                    </div>
                    <thead>
                      <tr>
                        <th>SL</th>
                        <th>Title</th>
                        <th>Link</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                        $i = 0;
                        foreach ($brands as $brand):
                            $i++;
                    ?>
                        <tr><!-- onclick="javascript: return confirm('Are you confirm delete record?')" -->
                            <td><input type="checkbox" name="dataArray[]" class="checkbox" value="<?= $brand->brand_id; ?>"> <?=$i;?></td>
                            <td><?= $brand->title; ?></td>
                           
                            <td>
                                <a href=""><?= $brand->link; ?></a>
                            </td>
                            <td>
                                <a title="View" class="btn btn-primary btn-sm text-white" href="show.php?id=<?= $brand->brand_id; ?>"><i class="far fa-eye"></i></a>
                                <a title="Restore" class="btn btn-success btn-sm text-white" href="restore.php?id=<?= $brand->brand_id; ?>"><i class="fas fa-trash-restore"></i></a>
                                <form action="delete.php" method="POST">
                                    <input type="hidden" value="<?= $brand->brand_id; ?>" name="deleteId">
                                    <button type="submit" title="Delete" class="btn btn-sm btn-danger text-white" onclick="javascript: return confirm('Are you confirm delete record?')"><i class="far fa-trash-alt"></i></button>
                                </form>
                            </td>
                        </tr>
                    <?php
                    endforeach;
                    ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
</form>

<?php 

  $index = ob_get_contents();
  ob_end_clean();
  echo str_replace("##MAIN_CONTENT##", $index, $layout)

 ?>