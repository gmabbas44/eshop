<?php $conn = new PDO("mysql:host=localhost;dbname=laracom","root",""); ?>

<?php
ob_start();
include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/admin/view/layout/index.php');
$layout = ob_get_contents();
ob_end_clean();
?>

<?php
ob_start();
?>

<?php
    use Eshop\Contact\Contact;
    use Eshop\Utility\Messages;
    $contact = new Contact;
// Selecting query
$query = "SELECT * FROM contact";
$sth = $conn->prepare($query);
$sth->execute();

$contacts = $sth->fetchAll(PDO::FETCH_ASSOC);
?>

        <div class="table-section shadow">
          <div class="container-fluid">
            <div class="row mb-3 overflow-hidden">
              <div class="col-8">
                <h2> View Contect</h2>
              </div>
              <div class="col-4">
                <a href ="create.php" class="main-button" >Add New</a>
              </div>
            </div>

            <?php if ($messages = Messages::get()):  ?>
                <div class="alert alert-success"><?= $messages; ?></div>
            <?php endif; ?>

            <div class="row">
              <div class="col-12">
                <div class="view-table">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>PhoneNumber</th>
                        <th>Comment</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach($contacts as $contact) { ?>
                        <tr>
                          <td><a href="show.php?id=<?php echo $contact['contact_id']; ?> "> <?php echo $contact['name']; ?> </a> </td>

                          <td><?php echo $contact['email']; ?></td>
                            <td><?php echo $contact['address']; ?></td>
                            <td><?php echo $contact['phoneNumber']; ?></td>
                            <td><?php echo $contact['comment']; ?></td>
                          <td><a href="edit.php?id=<?php echo $contact['contact_id']; ?>">Edit</a> | <a href="delete.php?id=<?php echo $contact['id']; ?>">Delete</a></td>
                        </tr>
                      <?php }  ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>


<?php

$add_product = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##", $add_product, $layout)

?>