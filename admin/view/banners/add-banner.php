<?php
    ob_start();
    include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/admin/view/layout/index.php');
    $layout = ob_get_contents();
    ob_end_clean();
    ?>



<?php 
    use Eshop\Utility\Messages;
    ob_start();
    
 ?>

        <div class="table-section shadow">
          <div class="container-fluid">
            <div class="row mb-3 overflow-hidden">
              <div class="col-8">
                <h2> Add Banner</h2>
              </div>
              <div class="col-4">
                <a href ="index.php" class="main-button" >View Products</a>
              </div>
            </div>
                <?php if ($massages = Messages::get()):  ?>
                    <div class="alert alert-success"><?=  $massages; ?></div>
                <?php endif; ?>
            <div class="row">
              <div class="col-12">
                <div class="add-form">
                  <form id="contact-form" method="post" enctype="multipart/form-data" action="store.php" role="form">

                    <div class="messages"></div>                    
                    <div class="row">

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input id="title"  value="" type="text" name="title" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="picture">Picture</label>
                                <input id="picture"  value="" type="file" name="picture" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="promotional_messages">Promotional Messages</label>
                                <input id="promotional_messages"  value="" type="text" name="promotional_messages" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="isDraft">Is Draft</label>
                                <input id="isDraft"  value="1" type="checkbox" name="is_draft" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="isactive">Is Active</label>
                                <input id="isactive"  value="1" type="checkbox" name="is_active" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-info">Save</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

       
<?php 

  $add_product = ob_get_contents();
  ob_end_clean();
  echo str_replace("##MAIN_CONTENT##", $add_product, $layout)

 ?>