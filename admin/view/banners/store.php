<?php include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/bootstrap.php'); ?>

<?php

	use Eshop\Banner\Banner;
	use Eshop\Utility\Messages;
	$banner = new Banner();
	if (isset($_POST)&& !empty($_POST))
	{
		$imgPath 	= DOCROOT.'/eshop/img/';
		$data 		= $_POST;
		$ImgName  	= $_FILES['picture']['name'];
		$ImageTmp 	= $_FILES['picture']['tmp_name'];

		$imgExt 	= pathinfo($ImgName, PATHINFO_EXTENSION);
		$data['picture'] = time().'_'.uniqid().'.'.$imgExt;
		
		if ($banner->store($data)) 
		{
			move_uploaded_file($ImageTmp, $imgPath.$data['picture']);
			Messages::set('Banner has been added successfully');
			header("location: index.php");
		}
		else
		{
			Messages::set('Sorry!.. There is a problem. Please try again');
			header("location: add-banner.php");
		}
	}
	else{
		header("location: add-banner.php");
	}


?>

