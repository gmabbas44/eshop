<?php
    ob_start();
    include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/admin/view/layout/index.php');
    $layout = ob_get_contents();
    ob_end_clean();
?>
<?php 
    use Eshop\Product\Product;
    $products = new Product();
    ob_start();
 ?>
<?php
    if (empty($_GET['id'])) {
        header('location: index.php');
    }
    $id = $_GET['id'];
    $product = $products->show($id);
?>

<div class="table-section shadow">
    <div class="container-fluid">
        <div class="row mb-3 overflow-hidden">
            <div class="col-8">
                <h2 class="display-5"> View Product</h2>
            </div>
            <div class="col-4">
                <a href ="index.php" class="main-button" >Go Back</a>
            </div>
        </div>
    <div class="row">
        <div class="col-md-6">
            <div class="view-details">
                <img class="w-100" src="<?= IMG . $product->picture;?>" alt="banner"> 
            </div>
            </div>
        
            <div class="col-md-6">
                
                <h2><?= $product->title;?></h2>
                <p><?= $product->description;?></p>
                <table class="table table-sm table-striped table-bordered">
                    <tr>
                        <th>Total Sales</th>
                        <td><?= $product->total_sales;  ?></td>
                    </tr>
                    <tr>
                        <th>Cost</th>
                        <td><?= $product->cost;  ?></td>
                    </tr>
                    <tr>
                        <th>MRP</th>
                        <td><?= $product->mrp;  ?></td>
                    </tr>
                    <tr>
                        <th>Is Active</th>
                        <td>
                            <?php 
                                if ($product->is_active == 1) {
                                    echo '<sapn  class="text-info"><i class="far fa-check-circle"></i> Yes<sapn>';
                                }else{
                                    echo '<sapn class="text-danger"><i class="far fa-times-circle"></i> No<sapn>';
                                }
                             ?>  
                        </td>
                    </tr>
                    <tr>
                        <th>Is draft</th>
                        <td>
                            <?php 
                                if ($product->is_draft == 1) {
                                    echo '<sapn  class="text-info"><i class="far fa-check-circle"></i> Yes<sapn>';
                                }else{
                                    echo '<sapn class="text-danger"><i class="far fa-times-circle"></i> No<sapn>';
                                }
                             ?>
                        </td>
                    </tr>
                </table>
                    
            </div>
        
    </div>
    </div>
</div>
  
        
<?php 

  $edit_product = ob_get_contents();
  ob_end_clean();
  echo str_replace("##MAIN_CONTENT##", $edit_product, $layout)

 ?>