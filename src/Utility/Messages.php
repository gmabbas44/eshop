<?php 

namespace Eshop\Utility;


class Messages
{
	public static function get()
	{
		if (array_key_exists('messages', $_SESSION) && !empty($_SESSION)) 
		{
			$messages = $_SESSION['messages'];
			$_SESSION['messages'] = "";
			return $messages;
		}
	}

	public static function set($messages)
	{
		$_SESSION['messages'] = $messages;
	}
}



 ?>