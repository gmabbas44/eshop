<?php

namespace Eshop\Category;

use Eshop\Db\Db;
use \PDO;
use Eshop\Utility\Messages;

class Category
{
	public $conn;
	public $category_id;
	public $name;
	public $link;
	public $soft_delete;
	public $is_draft;
	public $created_at;
	public $modified_at;

	function __construct(){
		$this->conn = Db::connect();
	}
    public function all()
    {
        $query = "SELECT * FROM  categories WHERE soft_delete = 0 ORDER BY category_id DESC";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $category =  $stmt->fetchAll(PDO::FETCH_OBJ);
        return $category;
    }

    public function store($data)
    {
    	$this->build($data);
    	$query = "INSERT INTO categories(name, link,is_draft) VALUES (:name, :link, :is_draft)";
		$stmt = $this->conn->prepare($query);
		$stmt->bindparam(":name",$this->name);
        $stmt->bindparam(":link",$this->link);
		$stmt->bindparam(":is_draft",$this->is_draft);
		$category = $stmt->execute();
        if ($category)
        {
            Messages::set('added successfully');
            header("location: index.php");
        }
        else
        {
            Messages::set('Sorry!.. There is a problem. Please try again');
            header("location: add-product.php");
        }
		return $category;
    }
    public function build($data)
    {
        $this->category_id  = $data['category_id'];
    	$this->name 		= $data['name'];
        $this->link         = $data['link'];
        $this->is_draft     = empty($data['is_draft']) ? 0 : $data['is_draft'];
    }

    public function show($id)
    {
    	$query = "SELECT * FROM  categories WHERE category_id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->bindparam(":id",$id);
        $stmt->execute();
        $category =  $stmt->fetch(PDO::FETCH_OBJ);
        if (!$category)
        {
        	Messages::set('Sorry!.. not found');
        	header('location: index.php');
        }
        return $category;
    }

    public function delete($id)
    {
		$query = "DELETE FROM categories WHERE category_id = :id";
		$results = $this->conn->prepare($query);
		$results->bindparam(":id",$id);
		$delete =  $results->execute();
		if ($delete) 
		{
			Messages::set('Category has been deleted');
        	header('location: index.php');
		}
		else
		{
			Messages::set('Sorry!.. There is a problem');
        	header('location: index.php');
		}
		return $delete;
    }

    public function softDelete($id)
    {
    	$query = "UPDATE categories SET soft_delete = 1 WHERE category_id = :id";
    	$stmt = $this->conn->prepare($query);
		$stmt->bindparam(":id",$id);
		$sd = $stmt->execute();
		if ($sd) {
			Messages::set(' Removed successfully..');
        	header('location: index.php');
		}
		else
		{
			Messages::set('Sorry!.. There is a problem');
        	header('location: index.php');
		}
        return $sd;

    }

    public function update($date)
    {
        $this->build($date);

        $query = "UPDATE `categories` SET `name`= :name,`link`=:link, is_draft = :is_draft  WHERE `category_id`= :id";
        
        $stmt = $this->conn->prepare($query);
        $stmt->bindparam(":name",$this->name);
        $stmt->bindparam(":link",$this->link);
        $stmt->bindparam(":is_draft",$this->is_draft);
        $stmt->bindparam(":id",$this->category_id);
        $update = $stmt->execute();

        if ($update) {
            Messages::set('Category has been update successfully');
            header("location: index.php");
        }else{
            Messages::set('Sorry!.. There is a problem. Please try again');
            header("location: index.php");
        }
        return $update;
    }

    public function trash()
    {
        $query = "SELECT * FROM  categories WHERE soft_delete = 1 ORDER BY 	category_id DESC";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $trash =  $stmt->fetchAll(PDO::FETCH_OBJ);
        return $trash;
    }
    
    public function restore($id)
    {
        $query = "UPDATE products SET soft_delete = 0 WHERE product_id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->bindparam(":id",$id);
        $sd = $stmt->execute();
        if ($sd) {
            Messages::set('Product restore successfully..');
            header('location: index.php');
        }
        else
        {
            Messages::set('Sorry!.. There is a problem');
            header('location: trash.php');
        }
    }



}